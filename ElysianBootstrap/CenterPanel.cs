﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap
{
    public class CenterPanel : Elisyan.Content.Content
    {
        public Content Main { get { return _Main; } set { _Main = value; } }

        Content _Main = null;
        public override string ToString(Session Session)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class=\"row\">");
            builder.Append("<div class=\"col-xs-1\">");
            builder.Append("</div>");
            builder.Append("<div class=\"col-xs-10\">");
            if (_Main != null) { builder.Append(_Main.ToString(Session)); }
            builder.Append("</div>");
            builder.Append("<div class=\"col-xs-1\">");
            builder.Append("</div>");
            builder.Append("</div>");
            return builder.ToString();

        }
    }
}
