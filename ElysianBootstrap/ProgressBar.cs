﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap
{
    public class ProgressBar : Content
    {
        string _Text = "";
        public string Text { get { return _Text; } set { _Text = value; } }
        int _Value = 0;
        public int Value { get { return _Value; } set { _Value = value; if (_Value < 0) { _Value = 0; } if (_Value > 100) { _Value = 100; } } }
        Type _Type = Type.Default;
        public Type Type { get { return _Type; } set { _Type = value; } }
        bool _Striped = false;
        public bool Striped { get { return _Striped; } set { _Striped = value; } }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            if (_Striped)
            { builder.Append("<div class=\"progress progress-striped active\">"); }
            else
            { builder.Append("<div class=\"progress\">"); }
            string extra = "";
            switch (_Type)
            {
                case Type.Danger: extra = " progress-bar-danger"; break;
                case Type.Warning: extra = " progress-bar-warning"; break;
                case Type.Success: extra = " progress-bar-success"; break;
                case Type.Info: extra = " progress-bar-info"; break;
            }
            builder.Append("<div class=\"progress-bar" + extra + "\" role=\"progressbar\" aria-valuenow=\"" + _Value.ToString() + "\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: " + _Value.ToString() +  "%;\">");
            builder.Append("<span class=\"sr-only\">" + _Text + "</span></div>");
            builder.Append("</div>");
            return builder.ToString();
        }
    }
}
