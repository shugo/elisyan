﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap
{
    public class FormTextbox : Content
    {
        public string Text { get; set; }
        public string Info { get; set; }
        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; } }
        public bool _Hidden = false;
        public bool Hidden { get { return _Hidden; } set { _Hidden = value; } }
        public override string ToString(Session Session)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class=\"input-group\">");
            if (Info != "")
            {
                builder.Append("<span class=\"input-group-addon\">");
                builder.Append(Info);
                builder.Append("</span>");
            }
            if (!_Hidden)
            {
                builder.Append("<input type=\"text\" name=\"");
            }
            else
            {
                builder.Append("<input type=\"password\" name=\"");
            }
            builder.Append(_Name);
            builder.Append("\" class=\"form-control\" placeholder=\"");
            builder.Append(Text);
            builder.Append("\"></div>");
            return builder.ToString();
        }
    }
}
