﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap
{
    public class Alert : Elisyan.Content.Content
    {
        public Content Content { get; set; }
        public Type Type { get; set; }
        public override string ToString(Session Session)
        {
            StringBuilder builder = new StringBuilder();
            switch (Type)
            {
                case Type.Danger: builder.Append("<div class=\"alert alert-danger\">"); break;
                case Type.Warning: builder.Append("<div class=\"alert alert-warning\">"); break;
                case Type.Success: builder.Append("<div class=\"alert alert-success\">"); break;
                default: builder.Append("<div class=\"alert alert-info\">"); break;
            }
            builder.Append(Content.ToString(Session));
            builder.Append("</div>");
            return builder.ToString();
        }
    }
}
