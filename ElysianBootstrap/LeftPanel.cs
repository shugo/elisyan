﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap
{
    public class LeftPanel : Elisyan.Content.Content
    {
        Content _Left = null;
        public Content Left { get { return _Left; } set { _Left = value; } }
        public Content Main { get { return _Main; } set { _Main = value; } }

        Content _Main = null;
        public override string ToString(Session Session)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class=\"row\">");
            builder.Append("<div class=\"col-xs-2\">");
            if (_Left != null) { builder.Append(_Left.ToString(Session)); }
            builder.Append("</div>");
            builder.Append("<div class=\"col-xs-10\">");
            if (_Main != null) { builder.Append(_Main.ToString(Session)); }
            builder.Append("</div>");
            builder.Append("</div>");
            return builder.ToString();

        }
    }
}
