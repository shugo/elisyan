﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap
{
    public class Quote : Elisyan.Content.Content
    {
        string _Quote = "";
        string _Authors = "";
        public string Text { get { return _Quote; } set { _Quote = value; } }
        public string Authors { get { return _Authors; } set { _Authors = value; } }
        public override string ToString()
        {
            return "<blockquote>" + "<p>" + _Quote + "</p><small>" + Authors + "</small></blockquote>" ;
        }
    }
}
