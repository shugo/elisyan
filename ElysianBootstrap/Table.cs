﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap
{
    public class Table : Elisyan.Content.Content
    {
        List<Elisyan.Content.Content> _Content = new List<Elisyan.Content.Content>();
        public List<Elisyan.Content.Content> Columns { get { return _Content; } }

        public class Row
        {
            List<Elisyan.Content.Content> _Content = new List<Elisyan.Content.Content>();
            public List<Elisyan.Content.Content> Columns { get { return _Content; } }
            Type _Type = Type.Default;
            public Type Type { get { return _Type; } set { _Type = value; } }
            public string ToString(Session Session)
            {
                StringBuilder builder = new StringBuilder();
                switch (_Type)
                {
                    case Type.Danger: builder.Append("<tr class=\"danger\">"); break;
                    case Type.Success: builder.Append("<tr class=\"success\">"); break;
                    case Type.Warning: builder.Append("<tr class=\"warning\">"); break;
                    default: builder.Append("<tr>"); break;
                }
                foreach (Elisyan.Content.Content element in _Content)
                {
                    builder.Append("<td>" + element.ToString(Session) + "</td>");
                }
                builder.Append("</tr>");
                return builder.ToString();
            }

            public override string ToString()
            {
                return ToString(null);
            }
        }
        List<Row> _Rows = new List<Row>();
        public List<Row> Rows { get { return _Rows; } }
        public override string ToString(Session Session)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<table class=\"table\">");
            foreach (Elisyan.Content.Content element in _Content)
            {
                builder.Append("<td>" + element.ToString(Session) + "</td>");
            }
            for (int n = 0; n < _Rows.Count; n++)
            {
                builder.Append(_Rows[n].ToString(Session));
            }
            builder.Append("</table>");
            return builder.ToString();
        }
    }
}
