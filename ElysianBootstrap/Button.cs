﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap
{
    public class Button : Elisyan.Content.Content
    {
        string _Text = "Button";
        public string Text { get { return _Text; } set { _Text = value; } }
        public Action.Action Action { get; set; }
        public Button() { }
        Elisyan.Content.Bootstrap.Type _Type = Type.Default;
        Elisyan.Content.Bootstrap.Type Type { get { return _Type; } }



        public override string ToString(Session Session)
        {
            string value = "";
            switch (_Type)
            {
                case Bootstrap.Type.Danger: value = "btn-danger"; break;
                case Bootstrap.Type.Default: value = "btn-default"; break;
                case Bootstrap.Type.Info: value = "btn-info"; break;
                case Bootstrap.Type.Primary: value = "btn-primary"; break;
                case Bootstrap.Type.Success: value = "btn-success"; break;
                case Bootstrap.Type.Warning: value = "btn-warning"; break;

                default: value = "btn-default"; break;
            }
            if (Action != null && Session != null)
            {
                string action = Session.RegisterAction(Action);
                if (Action is Action.ClientAction)
                {
                    return "<button type=\"button\" class = \"btn " + value + "\" onclick=\"" + Action.ToString() + "\">" + _Text + "</button>";

                }
                else
                {
                    return "<button type=\"button\" class = \"btn " + value + "\" onclick=\"var x;x=new XMLHttpRequest();x.open('get', '" + action + "', true);x.send();\"><span class=\"glyphicon glyphicon-star\"></span> Star" + _Text + "</button>";

                }
            }
            else
            {
                return "<button type=\"button\" class = \"btn " + value + "\">" + _Text + "</button>";
            }
        }
 
    }
}
