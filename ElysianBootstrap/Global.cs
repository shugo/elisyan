﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Bootstrap
{
    static public class Global
    {
        static Elisyan.Style _BootstrapDefaultStyle = new Elisyan.Style(ElysianBootstrap.Properties.Resources.BootstrapCssMin);
        static Elisyan.Script _BootstrapDefaultScript = new Elisyan.Script(ElysianBootstrap.Properties.Resources.BootstapJsMin);
        static public Elisyan.Style BootstrapDefaultStyle() { return _BootstrapDefaultStyle; }
        static public Elisyan.Script BootstrapDefaultScript() { return _BootstrapDefaultScript; }

    }
}
