﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap.Dynamic
{
    public class JobProgressBar : Elisyan.Content.Dynamic.DynamicContent
    {
        Elisyan.Job.Status _JobStatus = null;
        Page _Page = new Page();
        Bootstrap.ProgressBar _ProgressBar = new ProgressBar();
        public JobProgressBar(Elisyan.Job.Status JobStatus)
        { _JobStatus = JobStatus; _Page.Body = _ProgressBar; }

        class SharedClientAction : Action.ClientAction
        {
            Page _Page = null;
            string _ID = "";
            Bootstrap.ProgressBar _ProgressBar;
            Elisyan.Job.Status _JobStatus;
            public SharedClientAction(Page Page, Bootstrap.ProgressBar ProgressBar, Elisyan.Job.Status JobStatus, string ID)
            {
                _ProgressBar = ProgressBar;
                _JobStatus = JobStatus;
                _Page = Page;
                _ID = ID;
            }
            public override string ToString(Session Session)
            {
                string action = Session.RegisterAction(new Elisyan.Action.Action(() => { _ProgressBar.Value = _JobStatus.Completed; return _Page; }));
                return "{var x;x=new XMLHttpRequest();x.onreadystatechange=function(){if(x.readyState==4 && x.status==200){document.getElementById('" + _ID + "').innerHTML=x.responseText}};x.open('get', '/" + action + "', true);x.send();}";
            }
            public override string ToString()
            {
                return "";
            }
        }

        protected override void OnRegister(Page Page)
        {

            Page.AddTimer(TimeSpan.FromMilliseconds(500), new SharedClientAction(_Page,  _ProgressBar, _JobStatus, ID)); 
        }

        public override string ToString(Session Session)
        {
            _ProgressBar.Value = _JobStatus.Completed;
            return "<div id=\"" + ID + "\">" + _Page.ToString(Session) + "</div>";
        }
    }
}
