﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Bootstrap.Dynamic
{
    public class Modal : Elisyan.Content.Dynamic.DynamicContent
    {
        string _Title = "";
        public string Title { get{ return _Title; } set {_Title = value;} }
        public Elisyan.Content.Content _Content = null;
        public Elisyan.Content.Content Content { get { return _Content; } set { _Content = value; } }
        public Elisyan.Action.Action ActionShow() { return new Elisyan.Action.ClientAction("$('#" + ID + "').modal('show')"); }
        public Elisyan.Action.Action ActionHide() { return new Elisyan.Action.ClientAction("$('#" + ID + "').modal('hide')"); }


        public override string ToString(Elisyan.Session Session)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class=\"modal fade bs-modal-sm\" id=\"" + ID +  "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"");
            builder.Append(_Title);
            builder.Append("\" aria-hidden=\"true\">");
            builder.Append("<div class=\"modal-dialog modal-sm\">");
            builder.Append("<div class=\"modal-content\">");
            builder.Append(_Content.ToString(Session));
            builder.Append("</div>");
            builder.Append("</div>");
            builder.Append("</div>");
            return builder.ToString();
        }
    }
}
