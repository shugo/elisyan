﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Authentication
{
    public class Group
    {
        Dictionary<string, User> _Users = new Dictionary<string, User>();
        Dictionary<string, Group> _Subgroup = new Dictionary<string, Group>();
        string _Name = "";
        public string Name { get { return _Name; } }
        public bool AddSubGroup(Group Group)
        {
            lock (_Subgroup)
            {
                if (_Subgroup.ContainsKey(Group.Name)) { return false; }
                _Subgroup.Add(_Name, Group);
                return true;
            }
        }

        public bool AddUser(User User)
        {
            lock (_Users)
            {
                if (_Users.ContainsKey(User.Name)) { return false; }
                _Users.Add(User.Name, User);
                return true;
            }
        }

        public bool RemoveUser(User User)
        {
            lock (_Users)
            {
                if (!_Users.ContainsKey(User.Name)) { return false; }
                _Users.Remove(User.Name);
            }
            return true;
        }

        public bool ContainUser(string Username) 
        {
            lock (_Users)
            {
                if (_Users.ContainsKey(Username)) { return true; }
            }
            lock (_Subgroup)
            {
                foreach (Group g in _Subgroup.Values)
                {
                    if (g.ContainUser(Username)) { return true; }
                }
            }
            return false;
            
        }

        public Token Authenticate(string Username, string Password)
        {
            lock (_Users)
            {
                if (_Users.ContainsKey(Username)) { return _Users[Username].Authenticate(Username, Password); }
            }
            lock (_Subgroup)
            {
                foreach (Group g in _Subgroup.Values)
                {
                    if (g.ContainUser(Username)) { return g.Authenticate(Username, Password); }
                }
            }
            return null;
        }
    }
}
