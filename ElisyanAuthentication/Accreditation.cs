﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  Elisyan.Authentication
{
    public class Accreditation
    {
        List<Group> _AllowedGroups = new List<Group>();
        List<Group> _RestrictedGroups = new List<Group>();
        List<User> _AllowedUsers = new List<User>();
        List<User> _RestrictedUsers = new List<User>();

        bool _AllowAnonymousUser = false;

        public bool IsAccredited(Token token)
        {
            if (token == null)
            {
                return _AllowAnonymousUser;
            }
            bool allowed = false;
            lock (_AllowedGroups)
            {
                if (_AllowedGroups.Count != 0)
                {
                    foreach (Group g in _AllowedGroups)
                    {
                        if (g.ContainUser(token.User.Name)) { allowed = true; break; }
                    }
                }
                else
                {
                    allowed = _AllowedUsers.Count == 0;
                }
            }
            if (!allowed)
            {
                lock (_AllowedUsers)
                {
                    foreach (User u in _AllowedUsers)
                    {
                        if (u.Name == token.User.Name) { allowed = true; break; }
                    }
                }
            }

            if (allowed)
            {
                lock (_RestrictedGroups)
                {
                    if (_RestrictedGroups.Count != 0)
                    {
                        foreach (Group g in _RestrictedGroups)
                        {
                            if (g.ContainUser(token.User.Name)) { return false; }
                        }
                    }
                }
                lock (_RestrictedUsers)
                {
                    if (_RestrictedUsers.Count != 0)
                    {
                        foreach (User u in _RestrictedUsers)
                        {
                            if (u.Name == token.User.Name) { return false; }
                        }
                    }
                }
            }
            return false;
        }
    }
}
