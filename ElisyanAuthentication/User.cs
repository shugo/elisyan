﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Authentication
{
    public class User
    {
        string _Username = "";
        byte[] _PasswordHash = null;
        string _ServerName = "";
        static System.Security.Cryptography.SHA512 sha512 = System.Security.Cryptography.SHA512.Create();

        public string Name { get { return _Username; } }

        public Token Authenticate(string Username, string Password)
        {
            string value = Password + ":" + _ServerName + ":" + Username;
            byte[] hash = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(value));
            return Authenticate(Username, hash);
        }
        public Token Authenticate(string Username, byte[] PasswordHash)
        {
            if (_Username != Username) { return null; }
            if (PasswordHash.Length != _PasswordHash.Length) { return null; }
            for (int n = 0; n < PasswordHash.Length; n++)
            { if (PasswordHash[n] != _PasswordHash[n]) { return null; } }
            Token token = new Token();
            token._User = this;
            return token;
        }
    }
}
