﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan
{
    public class Job
    {
        public class Status
        {
            internal Status() { }

            public enum State
            {
                Waiting,
                Starting,
                Running,
                Ended
            }
            internal bool _Expired = false;
            public bool Expired 
            {
                get { return _Expired; }
                set 
                {
                    if (!_Expired)
                    {
                        _Expired = value;
                    }
                }
            }
            internal bool _Started = false;
            internal Job _Parent = null;
            State _CurrentState = State.Starting;
            volatile int _Completed = 0;
            public State CurrentState { get { return _CurrentState; } set { _CurrentState = value; } }
            public int Completed { get { return _Completed; } set { _Completed = value; } }
            
        }
        public delegate void Action();
        public delegate void ActionWithParameter(object Parameter);
        public delegate void ActionWithInfo(ref Status Status);
        public delegate void ActionWithParameterWithInfo(object Parameter, ref Status Status);



        public static void CreateShortJob(Action Action)
        {
            JobPool.AddShortJob(new Job(Action));
        }
        public static void CreateLongJob(Action Action)
        {
            JobPool.AddLongJob(new Job(Action));
        }

        public enum CreationMethod
        {
            Create,
            ReplaceIfNotStarted,
        };

        public static Status CreateTimer(ActionWithInfo Action, int Interval, bool Repeat)
        {
            Job Job = new Job(Action);
            JobPool.AddTimer(Job, Interval, Repeat);
            return Job;
        }

        public static Status CreateTimer(ActionWithParameterWithInfo Action, object Parameter, int Interval, bool Repeat)
        {
            Job Job = new Job(Action, Parameter);
            JobPool.AddTimer(Job, Interval, Repeat);
            return Job;
        }

        public static Status CreateLongJob(ActionWithInfo Action)
        {
            return CreateLongJob(Action, null, CreationMethod.Create);

        }
        public static Status CreateLongJob(ActionWithParameterWithInfo Action, object Parameter)
        {
            return CreateLongJob(Action, null, Parameter, CreationMethod.Create);
        }

        public static Status CreateLongJob(ActionWithInfo Action, Status Target, CreationMethod CreationMethod)
        {
            switch (CreationMethod)
            {
                case CreationMethod.Create:
                    {
                        Job j = new Job(Action);
                        JobPool.AddLongJob(j);
                        return j._Status;
                    }
                case CreationMethod.ReplaceIfNotStarted:
                    {
                        lock (Target)
                        {
                            if (!Target._Started)
                            {
                                Target._Parent._Action = null;
                                Target._Parent._ActionWithInfo = Action;
                                Target._Parent._ActionWithParam = null;
                                Target._Parent._ActionWithParamWithInfo = null;
                                Target._Parent._Parameter = null;
                                return Target;
                            }
                        }
                        return CreateLongJob(Action, Target, CreationMethod.Create);
                    }
            }
            return null;
        }
        public static Status CreateLongJob(ActionWithParameterWithInfo Action, Status Target, object Parameter, CreationMethod CreationMethod)
        {
            switch (CreationMethod)
            {
                case CreationMethod.Create:
                    {
                        Job j = new Job(Action, Parameter);
                        JobPool.AddLongJob(j);
                        return j._Status;
                    }
                    break;
                case CreationMethod.ReplaceIfNotStarted:
                    {
                        lock (Target)
                        {
                            if (!Target._Started)
                            {
                                Target._Parent._Action = null;
                                Target._Parent._ActionWithInfo = null;
                                Target._Parent._ActionWithParam = null;
                                Target._Parent._ActionWithParamWithInfo = Action;
                                Target._Parent._Parameter = Parameter;
                                return Target;
                            }
                        }
                        return CreateLongJob(Action, Target, Parameter, CreationMethod.Create);
                    }
                    break;
            }
            return null;
        }


        public static void CreateShortJob(ActionWithParameter Action, object Parameter)
        {
            JobPool.AddShortJob(new Job(Action, Parameter));
        }
        public static void CreateLongJob(ActionWithParameter Action, object Parameter)
        {
            JobPool.AddLongJob(new Job(Action, Parameter));
        }

        Action _Action = null;
        ActionWithParameter _ActionWithParam = null; object _Parameter = null;
        ActionWithParameterWithInfo _ActionWithParamWithInfo = null;
        ActionWithInfo _ActionWithInfo = null;

        internal Status _Status = new Status();

        internal Job(Action Action) { _Action = Action; }
        internal Job(ActionWithParameter Action, object Parameter) { _Parameter = Parameter; _ActionWithParam = Action; }
        internal Job(ActionWithParameterWithInfo Action, object Parameter) { _Parameter = Parameter; _ActionWithParamWithInfo = Action; _Status = new Status(); }
        internal Job(ActionWithInfo Action) { _ActionWithInfo = Action; _Status = new Status(); }

        internal void Run()
        {
            if (_Status._Expired) { return; }
            if (_ActionWithParam != null) { _ActionWithParam(_Parameter); }
            else if (_ActionWithInfo != null) 
            {
                lock (_Status) { _Status.CurrentState = Status.State.Running; _Status._Started = true; } _ActionWithInfo(ref _Status);
            }
            else if (_ActionWithParamWithInfo != null) 
            {
                lock (_Status) { _Status.CurrentState = Status.State.Running; _Status._Started = true; } _ActionWithParamWithInfo(_Parameter, ref _Status);
            }
            else { _Action(); }
        }
    }
}
