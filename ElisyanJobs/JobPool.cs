﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan
{
    static class JobPool
    {
        static System.Threading.Thread[] _LongJobThreads = null;
        static System.Threading.Thread[] _ShortJobThreads = null;
        static System.Threading.Thread _Manager = null;

        static Queue<Job> _LongJobs = new Queue<Job>();
        static Queue<Job> _ShortJobs = new Queue<Job>();


        static internal void AddShortJob(Job Job) 
        {
            lock (_ShortJobs)
            {
                _ShortJobs.Enqueue(Job);
                System.Threading.Monitor.Pulse(_ShortJobs);

            }
        }
        static internal void AddLongJob(Job Job) 
        {
            lock (_LongJobs)
            {
                _LongJobs.Enqueue(Job);
                System.Threading.Monitor.Pulse(_LongJobs);
            }
        }
        static internal void AddTimer(Job Job, int Interval, bool Repeat)
        {
            Timer Timer = new Timer();
            Timer.Job = Job;
            Timer.Periodic = Repeat;
            Timer.RemainingTime = Interval;
            Timer.Interval = Interval;
            lock (_Timers)
            {
                _Timers.Add(Timer);
            }
        }

        static JobPool()
        {
            int processor = System.Environment.ProcessorCount;
            _LongJobThreads = new System.Threading.Thread[processor];
            for (int n = 0; n < processor; n++)
            {
                _LongJobThreads[n] = new System.Threading.Thread(RunLongJob) { IsBackground = true };
                _LongJobThreads[n].Start();
            }
            _ShortJobThreads = new System.Threading.Thread[processor];
            for (int n = 0; n < processor; n++)
            {
                _ShortJobThreads[n] = new System.Threading.Thread(RunShortJob) { IsBackground = true };
                _ShortJobThreads[n].Start();
            }

            _Manager = new System.Threading.Thread(TimerManager);
            _Manager.Start();
        }

        internal class Timer
        {
            internal int RemainingTime = 0;
            internal int Interval = 0;
            internal Job Job = null;
            internal bool Periodic = true;
        }

        static List<Timer> _Timers = new List<Timer>();

        static internal void TimerManager()
        {
            DateTime OldDateTime = DateTime.Now;
            while (true)
            {
                try
                {
                    DateTime NewDateTime = DateTime.Now;
                    int Interval = (int)((OldDateTime - NewDateTime).TotalMilliseconds);

                    lock (_Timers)
                    {
                        
                        bool MustCollect = false;
                        foreach (Timer timer in _Timers)
                        {
                            timer.RemainingTime -= Interval;
                            while (timer.RemainingTime <= 0 && !timer.Job._Status._Expired)
                            {
                                AddLongJob(timer.Job);
                                if (timer.Periodic)
                                {
                                    /* The user want a continiously running job */
                                    /* I don't know how to handle this case */
                                    if (timer.Interval <= 0) { break; }
                                    timer.RemainingTime += timer.Interval;
                                }
                                else
                                {
                                    MustCollect = true;
                                    break;
                                }
                            }
                            if (timer.Job._Status._Expired)
                            {
                                MustCollect = true;
                            }
                        }
                        if (MustCollect)
                        {
                            for (int n = 0; n < _Timers.Count; n++)
                            {
                                if (
                                     (!_Timers[n].Periodic && _Timers[n].RemainingTime < 0) ||
                                     (_Timers[n].Job._Status._Expired)
                                   )
                                {
                                    _Timers.RemoveAt(n);
                                    n -= 1;
                                }
                            }
                        }
                    }
                    OldDateTime = NewDateTime;
                }
                catch { }
                try
                {
                    System.Threading.Thread.Sleep(100);
                }
                catch { }
            }
        }

        static internal void RunLongJob()
        {
            while (true)
            {
                try
                {
                    Job CurrentJob = null;
                    lock (_LongJobs)
                    {
                        while (_LongJobs.Count == 0) { System.Threading.Monitor.Wait(_LongJobs); }
                        CurrentJob = _LongJobs.Dequeue();
                    }
                    if (CurrentJob != null) { CurrentJob.Run(); }
                }
                catch { }
            }
        }

        static internal void RunShortJob()
        {
            while (true)
            {
                try
                {
                    Job CurrentJob = null;
                    lock (_ShortJobs)
                    {
                        while (_ShortJobs.Count == 0) { System.Threading.Monitor.Wait(_ShortJobs); }
                        CurrentJob = _ShortJobs.Dequeue();
                    }
                    if (CurrentJob != null)
                    {
                        CurrentJob.Run();
                    }
                }
                catch { }
            }
        }
    }
}
