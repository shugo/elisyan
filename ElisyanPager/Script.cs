﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan
{
    public class Script
    {
        public Script(string Code) { _Code = Code ; }
        string _Code = "";
        public string Code { get { return _Code; } set { _Code = value; } }
        public override string ToString()
        {
            return _Code;
        }
    }
}
