﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Action
{
    public class ClientAction : Action
    {
        string _JavaScriptCode = "";
        public ClientAction() { }
        public ClientAction(string JavaScriptCode) { _JavaScriptCode = JavaScriptCode; }
        public string JavaScriptCode { get { return _JavaScriptCode; } set { _JavaScriptCode = value; } }
        public override Page Run(Dictionary<string, string> Parameters, Session Session) { return null; }
        public override string ToString(Session Session)
        {
            return ToString();
        }
        public override string ToString()
        {
            return _JavaScriptCode;
        }
    }
}
