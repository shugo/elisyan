﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Action
{
    public class Action
    {
        public delegate Page Act(Dictionary<string, string> Parameters, Session Session);
        public delegate void ActNoReturn(Dictionary<string, string> Parameters, Session Session);
        public delegate Page EmptyAct();
        public delegate void EmptyActNoReturn();

        Act _Act;
        public Action(Act Act) { _Act = Act; }
        public Action(EmptyAct Act) { _Act = (Parameters, Sesssion) => { return Act(); }; }
        public Action() { _Act = (Parameters, Session) => { return null; }; }
        public static implicit operator Action(Act Act) { return new Action(Act); }
        public static implicit operator Action(ActNoReturn Act) { return new Action((Parameters, Session) => { Act(Parameters, Session); return null; }); }
        public static implicit operator Action(EmptyAct Act) { return new Action((Parameters, Sesssion) => { return Act(); }); }
        public static implicit operator Action(EmptyActNoReturn Act) { return new Action((Parameters, Session) => { Act(); return null; }); }

        public virtual Page Run(Dictionary<string, string> Parameters, Session Session) { return _Act(Parameters, Session); }

        public virtual string ToString(Session Session)
        { return ToString(); }
    }
}
