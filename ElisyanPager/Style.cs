﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan
{
    public class Style
    {
        public Style() { }
        public Style(string Code) { _Value = Code; }
        string _Value = "";
        public string Code { get { return Code; } set { Code = value; } }
        public override string ToString()
        {
            return _Value;
        }
    }
}
