﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Dynamic
{
    public abstract class DynamicContent : Content
    {
        internal string _ID = "";
        internal bool _Registered = false;
        public string ID { get { return _ID; } set { _ID = value; } }
        protected virtual void OnRegister(Page Page) { }
        internal void ForceCallOnRegister(Page Page) { OnRegister(Page); }
    }
}
