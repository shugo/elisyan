﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Dynamic
{
    public class SubPage : DynamicContent
    {
        public SubPage() { }

        string _URL = "";
        public string URL { get { return _URL; } set { _URL = value; } }

        public Action.Action ActionNavigate(string URL)
        {
            string code = "{var x;x=new XMLHttpRequest();x.onreadystatechange=function(){if(x.readyState==4 && x.status==200){document.getElementById('" + ID + "').innerHTML=x.responseText}};x.open('get', '" + URL + "', true);x.send();}";
            return new Elisyan.Action.ClientAction(code) ;
        }

        public override string ToString(Session Session)
        {
            return "<div id=\"" + ID + "\"></div>";
        }
    }
}
