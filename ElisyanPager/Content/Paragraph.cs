﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    class Paragraph : Content
    {
        public enum Align
        {
            Center,
            Left,
            Right,
            Justified
        }
        Align _TextAlign = Align.Right;
        Align TextAlign { get { return _TextAlign; } set { _TextAlign = value; } }
        string _Value = "";
        public string Value { get { return _Value; } set { _Value = value; } }
        public override string ToString()
        {
            switch (_TextAlign)
            {
                case Align.Right: return "<p style=\"text-align:right\">" + _Value + "</p>"; break;
                case Align.Left: return "<p style=\"text-align:left\">" + _Value + "</p>"; break;
                case Align.Center: return "<p style=\"text-align:center\">" + _Value + "</p>"; break;
                case Align.Justified: return "<p style=\"text-align:justified\">" + _Value + "</p>"; break;
            }
            return "<p>" + _Value + "</p>";
        }
    }
}
