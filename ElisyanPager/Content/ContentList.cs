﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    public class ContentList : Content
    {
        List<Content> _List = new List<Content>();
        public List<Content> List { get { return _List; } }

        public override string ToString(Session Session)
        {
            StringBuilder builder = new StringBuilder();
            for (int n = 0; n < _List.Count; n++)
            {
                builder.Append(_List[n].ToString(Session));
            }
            return builder.ToString();
        }
    }
}
