﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    public class Button : Content
    {
        Page _Page = null;
        public Page Page { get { return _Page; } }
        Content _Content = null;
        public Content Content { get { return _Content; } set { _Content = value; } }

        public override string ToString()
        {
            if (Content == null) { return "<button/>"; }
            else { return "<button>" + _Content.ToString() + "</button>"; }
        }
    }
}
