﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    public class FormSubmit : Content
    {
        string _Text = "";
        public string Text { get { return _Text; } set { _Text = value; } }
        public override string ToString(Session Session)
        {
            return "<input type=\"submit\" value=\"" + _Text + "\">";
        }
    }
}
