﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    public class Form : Content
    {
        Content _Content = null;

        public Content Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        Action.Action _Action = null;
        public Action.Action Action { get { return _Action; } set { _Action = value; } }
        

        public override string ToString(Session Session)
        {
            StringBuilder builder = new StringBuilder();
            if (_Action != null && Session != null)
            {
                string id = Session.RegisterAction(_Action);
                builder.Append("<form action=\"/" + id + "\" method=\"post\">");
            }
            else
            {
                builder.Append("<form action=\"\" method=\"post\">");
            }
            if (_Content != null) { builder.Append(_Content.ToString(Session)); }
            builder.Append("</form>");
            return builder.ToString();
        }
    }
}
