﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    public class FormTextbox : Content
    {
        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; } }
        public bool _Hidden = false;
        public bool Hidden { get { return _Hidden; } set { _Hidden = value; } }
        public override string ToString(Session Session)
        {
            if (!_Hidden)
            {
                return "<input type=\"text\" name=\"" + _Name + "\"/>";
            }
            else
            {
                return "<input type=\"password\" name=\"" + _Name + "\"/>";
            }
        }
    }
}
