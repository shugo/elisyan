﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    public class Content
    {
        class GeneratorContext
        {
        }

        public static implicit operator Content(string Value)
        { Text text = new Text(); text.Value = Value; return text; }
        public static implicit operator Content(Content[] Array)
        {
            ContentList List = new ContentList();
            for (int n = 0; n < Array.Length; n++)
            {
                List.List.Add(Array[n]);
            }
            return List;
        }

        public virtual string ToString(Session Session)
        {
            return ToString();
        }

        public override string ToString()
        {
            return "";
        }
    }
}
