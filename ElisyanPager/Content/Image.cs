﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    public class Image : Content
    {
        static ulong _a_uid = 0;
        static ulong _b_uid = 0;
        static ulong _c_uid = 0;
        System.Drawing.Image _Image = null;

        static Dictionary<string, byte[]> _ImageData = new Dictionary<string, byte[]>();

        public System.Drawing.Image DisplayedImage
        {
            get { return _Image; }
            set { _Image = value; _Data = null; imageid = ""; }
        }

        static string GenUID(System.Drawing.Image Image)
        {
            if (_a_uid == ulong.MaxValue)
            {
                _a_uid = 0;
                if (_b_uid == ulong.MaxValue) { _b_uid = 0; _c_uid++; }
                else { _b_uid++; }

            }
            else { _a_uid++; }
            string uid = ".image." + _a_uid.ToString() + "." + _b_uid.ToString() + "." + _c_uid.ToString();
            System.IO.MemoryStream MemoryStream = new System.IO.MemoryStream();
            Image.Save(MemoryStream, System.Drawing.Imaging.ImageFormat.Png);
            MemoryStream.Flush();
            Console.WriteLine("/" + uid + " has been added in the cache");
            _ImageData.Add("/" + uid, MemoryStream.ToArray());
            return uid;
        }
        string imageid = "";
        string alt = "Elysian Image";
        byte[] _Data = null;

        public static byte[] GetDataFromImageId(string ImageId)
        {
            if (_ImageData.ContainsKey(ImageId)) { return _ImageData[ImageId]; }
            return null;
        }
        public override string ToString()
        {
            if (_Image != null)
            {
                if (imageid == "") { imageid = GenUID(_Image); }
                return "<img src=\"" + imageid + "\"alt=\"" + alt + "\"/>";
            }
            return "";
        }
    }
}
