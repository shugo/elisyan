﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content
{
    public class Text : Content
    {
        string _Value = "";
        public string Value { get { return _Value; } set { _Value = value; } }
        public override string ToString()
        {
            return _Value;
        }
    }
}
