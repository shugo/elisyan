﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan
{
    public class Session
    {
        string _UserName = "";
        public string UserName { get { return _UserName; } set { _UserName = value; } }
        Dictionary<string, object> _Variables = new Dictionary<string, object>();
        public Dictionary<string, object> Variables { get { return _Variables; } }
        bool _Expired = false;
        public bool Expired { get { return _Expired; } set { _Expired = value; } }
        Dictionary<string, Action.Action> _Actions = new Dictionary<string, Action.Action>();
        ulong _uid_action_a = 0;
        ulong _uid_action_b = 0;
        ulong _uid_action_c = 0;
        Authentication.Token _Token = null;
        public Authentication.Token Token { get { return _Token; } }
        public bool Authenticate(Authentication.Group Group, string Username, string Password)
        {
            Authentication.Token newtoken = Group.Authenticate(Username, Password);
            if (newtoken != null)
            {
                _Token = newtoken;
                return true;
            }
            return false;
        }

        public string RegisterAction(Action.Action Action)
        {
            lock (_Actions)
            {
                if (_uid_action_a == ulong.MaxValue)
                {
                    if (_uid_action_b == ulong.MaxValue)
                    {
                        if (_uid_action_c == ulong.MaxValue)
                        {
                            _Expired = true;
                            _uid_action_a = 0;
                            _uid_action_b = 0;
                            _uid_action_c = 0;
                        }
                        else
                        {
                            _uid_action_c++;
                        }
                    }
                    else
                    {
                        _uid_action_b++;
                    }
                }
                else
                {
                    _uid_action_a++;
                }
                string uid = ".session.method." + _uid_action_a.ToString() + "_" + _uid_action_b.ToString() + "_" + _uid_action_c.ToString();
                _Actions[uid] = Action;
                return uid;
            }
        }

        public string GenerateUID()
        {
            lock (this)
            {
                if (_uid_action_a == ulong.MaxValue)
                {
                    if (_uid_action_b == ulong.MaxValue)
                    {
                        if (_uid_action_c == ulong.MaxValue)
                        {
                            _Expired = true;
                            _uid_action_a = 0;
                            _uid_action_b = 0;
                            _uid_action_c = 0;
                        }
                        else
                        {
                            _uid_action_c++;
                        }
                    }
                    else
                    {
                        _uid_action_b++;
                    }
                }
                string uid =  "_" + _uid_action_a.ToString() + "_" + _uid_action_b.ToString() + "_" + _uid_action_c.ToString();
                return uid;
            }
        }

        public Page InvokeAction(string Name, Dictionary<string, string> Parameters)
        {
            if (_Actions.ContainsKey(Name)) { return _Actions[Name].Run(Parameters, this); }
            return null;
        }
    }
}
