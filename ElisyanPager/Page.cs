﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan
{
    public class Page
    {
        
        string _Title = "Elisyan Page";
        public string Title { get { return _Title; } set { _Title = value; } }
        public Page() { }
        Content.Content _Body = null;
        Content.Content _Head = null;
        public Content.Content Body { get { return _Body; } set { _Body = value; } }
        public Content.Content Head { get { return _Head; } set { _Head = value; } }
        ulong _dynamic_uid = 0;

        Authentication.Accreditation _Accreditation = null;
        public Authentication.Accreditation Accreditation { get { return _Accreditation; } set { _Accreditation = value; } }
        

        public Action.Action ActionNavigate(string URL)
        {
            return new Elisyan.Action.ClientAction("window.location='" + URL + "'");
        }

        class TimerEvent
        {
            internal Action.Action _Action;
            internal TimeSpan _Interval;
            public TimerEvent(TimeSpan Interval, Action.Action Action)
            {
                _Interval = Interval;
                _Action = Action;
            }
            public override string ToString()
            {
                return "";
            }
        }

        List<TimerEvent> _Timers = new List<TimerEvent>();

        public void AddTimer(TimeSpan Interval, Action.Action Action)
        {
            _Timers.Add(new TimerEvent(Interval, Action));
        }

        public Content.Dynamic.DynamicContent Register(Content.Dynamic.DynamicContent DynamicContent)
        {
            lock (this)
            {
                lock (DynamicContent)
                {
                    if (DynamicContent._Registered)
                    {
                        throw new Exception("A dynamic content can only be registered once");
                    }
                    _dynamic_uid++;
                    string uid = GetHashCode().ToString() + "_" + _dynamic_uid.ToString();
                    DynamicContent._Registered = true;
                    DynamicContent._ID = uid;
                    DynamicContent.ForceCallOnRegister(this);
                }
            }
            return DynamicContent;
        }

        public string ToString(Session Session)
        {
            if (_Accreditation != null)
            {
                if (!_Accreditation.IsAccredited(Session.Token))
                {
                    return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title>Denied</title></head><body>Denied</body></html>";
                }
            }
            StringBuilder builder = new StringBuilder();
            builder.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            builder.Append("<html>");
            builder.Append("<head><title>");
            builder.Append(_Title);
            builder.Append("</title>");
            if (_Head != null) { builder.Append(_Head.ToString(Session)); }

            if (_Timers.Count > 0)
            {
                builder.Append("<script>");
                for (int n = 0; n < _Timers.Count; n++)
                {
                    builder.Append("window.setInterval(function(){" + _Timers[n]._Action.ToString(Session) + "}, " + _Timers[n]._Interval.TotalMilliseconds.ToString() + ")");
                }
                builder.Append("</script>");
            }
            builder.Append("</head><body>");

            if (_Body != null) { builder.Append(_Body.ToString(Session)); }
            builder.Append("</body></html>");

            return builder.ToString();
        }
        public override string ToString()
        {
            return ToString(null);
        }
    }
}
