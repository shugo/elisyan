﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Content.Ace.Dynamic
{
    public class Editor : Elisyan.Content.Dynamic.DynamicContent
    {
        string _Code = "";
        public string Code { get { return _Code; } set { _Code = value; } }
        public enum Language
        {
            Javascript,
            CSharp,
            CPlusPlus,
            C,
            Python,
        }
        Language _Language = Language.CSharp;
        public Language EditorLanguage { get { return _Language; } set { _Language = value; } }
        public override string ToString(Session Session)
        {
            string URL = Session.RegisterAction(new Elisyan.Action.Action((Dictionary<string, string> Variables, Session session) => { if (Variables.ContainsKey("code")) { Console.WriteLine("update code" + _Code); _Code = Variables["code"]; } return null; }));
            string mode = "javascript";
            switch (_Language)
            {
                case Language.CSharp: mode = "csharp"; break;
                case Language.C: case Language.CPlusPlus: mode = "c-cpp"; break;
                case Language.Python: mode = "python"; break;
            }
            return "<div  id=\"" + ID + "\" style=\"height:800px\">" + _Code + "</div><script src=\"/ace-builds/src-noconflict/ace.js\" type=\"text/javascript\" charset=\"utf-8\"></script><script>{var editor = ace.edit(\"" + ID + "\"); editor.setTheme(\"ace/theme/monokai\"); editor.getSession().setMode(\"ace/mode/" + mode + "\"); editor.on('change', function(e) {var x;x=new XMLHttpRequest();x.onreadystatechange=function(){};x.open('post', '" + URL + "', true);x.send(encodeURI('code='+editor.getValue()));});}</script>";
        }
    }
}
