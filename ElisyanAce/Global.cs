﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Ace
{
    public static class Global
    {
        public static List<KeyValuePair<string, Script>> AceDefaultScripts()
        {
            List<KeyValuePair<string, Script>> Result = new List<KeyValuePair<string, Script>>();
            Type t = typeof(ElisyanAce.Properties.Resources);
            foreach (System.Reflection.PropertyInfo property in t.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.GetProperty))
            {
                try
                {
                    string name = property.Name.Replace("_", "-");
                    System.Reflection.MethodInfo Method = property.GetGetMethod(true);
                    string code = (string)Method.Invoke(null, null);
                    Result.Add(new KeyValuePair<string, Script>("/ace-builds/src-noconflict/" + name + ".js", new Script(code)));
                }
                catch { }
            }
            return Result;
        }
    }
}
