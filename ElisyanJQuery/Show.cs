﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Action.JQuery
{
    public class Show : Elisyan.Action.ClientAction
    {
        Content.Dynamic.DynamicContent _Target = null;
        public Show(Content.Dynamic.DynamicContent Target) { _Target = Target; }
        public override string ToString()
        {
            return "$('#" + _Target.ID + "').show()";
        }
    }
}
