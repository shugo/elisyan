﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.JQuery
{
    public static class Global
    {
        static Elisyan.Script _JQueryDefaultScript = new Elisyan.Script(ElisyanJQuery.Properties.Resources.JQuery);
        static public Elisyan.Script JQueryDefaultScript() { return _JQueryDefaultScript; }
    }
}
