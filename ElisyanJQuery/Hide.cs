﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Action.JQuery
{
    public class Hide : Elisyan.Action.ClientAction
    {
        Content.Dynamic.DynamicContent _Target = null;
        public Hide(Content.Dynamic.DynamicContent Target) { _Target = Target; }
        public override string ToString()
        {
            return "$('#" + _Target.ID + "').hide()";
        }
    }
}
