﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Network
{
    public class Server
    {
        System.Net.HttpListener _Listener = new System.Net.HttpListener();
        Elisyan.Page _MainPage = new Elisyan.Page();
        Elisyan.Page _PageNotFound = new Elisyan.Page();
        Elisyan.Page _ServerException = new Elisyan.Page();

        public Elisyan.Page MainPage { get { return _MainPage; } set { _MainPage = value; } }
        Dictionary<string, Elisyan.Page> _Pages = new Dictionary<string, Elisyan.Page>();
        Dictionary<string, Elisyan.Style> _Styles = new Dictionary<string, Elisyan.Style>();
        Dictionary<string, Elisyan.Script> _Scripts = new Dictionary<string, Elisyan.Script>();

        string[] _Hostname;

        public void Add(Elisyan.Page Page, string Location)
        {
            if (_Pages.ContainsKey(Location)) 
            {
                throw new Exception("A page as already been registered with this name");   
            }
            if (_Styles.ContainsKey(Location))
            {
                throw new Exception("A style as already been registered with this name");
            }
            if (_Scripts.ContainsKey(Location))
            {
                throw new Exception("A script as already been registered with this name");
            }
            _Pages.Add(Location, Page);

        }

        public void Add(Elisyan.Style Style, string Location)
        {
            if (_Pages.ContainsKey(Location))
            {
                throw new Exception("A page as already been registered with this name");
            }
            if (_Styles.ContainsKey(Location))
            {
                throw new Exception("A style as already been registered with this name");
            }
            if (_Scripts.ContainsKey(Location))
            {
                throw new Exception("A script as already been registered with this name");
            }
            _Styles.Add(Location, Style);
        }

        public void Add(Elisyan.Script Script, string Location)
        {
            if (_Pages.ContainsKey(Location))
            {
                throw new Exception("A page as already been registered with this name");
            }
            if (_Styles.ContainsKey(Location))
            {
                throw new Exception("A style as already been registered with this name");
            }
            if (_Scripts.ContainsKey(Location))
            {
                throw new Exception("A script as already been registered with this name");
            }
            _Scripts.Add(Location, Script);
        }

        public Server(params string[] hostname)
        {
            try
            {
                _PageNotFound = new Elisyan.Page();
                _PageNotFound.Title = "Page not found";
                _PageNotFound.Body = "Error 404: The specified page is not present on this server";

                _ServerException = new Elisyan.Page();
                _ServerException.Title = "Internal error";
                _ServerException.Body = "Error 500: Internal error (.NET Exception)";
            }
            catch { }

            _Hostname = hostname;
        }

        public void RunAsync()
        {
            System.Threading.Thread Thread = new System.Threading.Thread(Run);
            Thread.IsBackground = true;
            Thread.Start();
        }

        public void Run()
        {
            try
            {
                _Listener = new System.Net.HttpListener();
                foreach (string _ in _Hostname)
                {
                    _Listener.Prefixes.Add(_);
                }
                _Listener.Start();
                Console.WriteLine("Server started");
                    {
                        while (true)
                        {
                            try
                            {
                                Elisyan.Job.CreateShortJob((object OClient) =>
                                    {
                                        Client Client = (Client)OClient;
                                        try
                                        {
                                            if (Client.IsPostRequest)
                                            {
                                                Console.WriteLine("Post Request page: " + Client.RequestedPageName);
                                                if (Client.RequestedPageName.StartsWith("/.session.method."))
                                                {
                                                    Elisyan.Page p = Client.UserSession.InvokeAction(Client.RequestedPageName.Substring(1), Client.RequestVariables);
                                                    if (p == null) { Client.SendResponse(_MainPage.ToString(Client.UserSession)); }
                                                    else { Client.SendResponse(p.ToString(Client.UserSession)); }
                                                }
                                                else
                                                { Client.SendResponse(_MainPage.ToString(Client.UserSession)); }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Request page: " + Client.RequestedPageName);
                                                if (Client.RequestedPageName == "/")
                                                { Client.SendResponse(_MainPage.ToString(Client.UserSession)); }
                                                else
                                                {
                                                    if (Client.RequestedPageName.StartsWith("/.image."))
                                                    {
                                                        byte[] Data = Elisyan.Content.Image.GetDataFromImageId(Client.RequestedPageName);
                                                        if (Data != null)
                                                        {
                                                            Client.SendResponse(Data, "image/png");
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Image not found: " + Client.RequestedPageName);
                                                        }
                                                    }
                                                    else if (Client.RequestedPageName.StartsWith("/.session.method."))
                                                    {
                                                        Elisyan.Page page = Client.UserSession.InvokeAction(Client.RequestedPageName.Substring(1), Client.RequestVariables);
                                                        if (page == null) { Client.SendResponse(MainPage.ToString(Client.UserSession)); }
                                                        else { Client.SendResponse(page.ToString(Client.UserSession)); }
                                                    }
                                                    else if (_Pages.ContainsKey(Client.RequestedPageName)) { Client.SendResponse(_Pages[Client.RequestedPageName].ToString(Client.UserSession)); }
                                                    else if (_Styles.ContainsKey(Client.RequestedPageName)) { Client.SendResponse(_Styles[Client.RequestedPageName].ToString(), "text/css"); }
                                                    else if (_Scripts.ContainsKey(Client.RequestedPageName)) { Client.SendResponse(_Scripts[Client.RequestedPageName].ToString(), "application/javascript"); }
                                                    else { Client.SendResponse(_PageNotFound.ToString(Client.UserSession)); Console.WriteLine("Page not found " + Client.RequestedPageName); }
                                                }
                                            }
                                        }
                                        catch
                                        {
                                            Client.SendResponse(_ServerException.ToString());
                                        }
                                    }, new Client(_Listener.GetContext()));
                            }
                            catch { }
                        }

                    }
                    

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        
        
    }
}
