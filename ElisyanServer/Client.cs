﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elisyan.Network
{
    class Client
    {
        
        System.Net.HttpListenerContext _Context;
        string _RequestedPageName = "";
        string _RequestedUrl = "";
        bool _PostRequest = false;
        Elisyan.Session _UserSession = null;
        public Elisyan.Session UserSession { get { return _UserSession; } }
        static Random _Rand = new Random();
        static Dictionary<string, Elisyan.Session> _Sessions = new Dictionary<string, Elisyan.Session>();
        Dictionary<string, string> _Variables = new Dictionary<string, string>();
        public Dictionary<string, string> RequestVariables { get { return _Variables; } }
        public bool IsPostRequest { get { return _PostRequest; } set { _PostRequest = value; } }

        public string RequestedPageName { get { return _RequestedPageName; } }

        string _UID = "";
        public string UID
        {
            get 
            {
                if (_UID == "")
                {
                    string uid = _Context.Request.UserAgent + _Context.Request.UserHostAddress + _Context.Request.UserHostName;
                    byte[] data = System.Text.Encoding.UTF8.GetBytes(uid);
                    data = System.Security.Cryptography.SHA512.Create().ComputeHash(data);
                    string result = "";
                    for (int n = 0; n < data.Length; n++)
                    {
                        result += data[n].ToString("X");
                    }

                    _Rand.NextBytes(data);
                    for (int n = 0; n < data.Length; n++)
                    {
                        result += data[n].ToString("X");
                    }
                    _UID = result;
                }
                    return _UID;
                
            }
        }

        public Client(System.Net.HttpListenerContext Context)
        {

            _Context = Context;
            string sessionid = "";
            try
            {
                System.Net.Cookie cookie = _Context.Request.Cookies["sessionid"];
                if (cookie != null)
                {
                    sessionid = cookie.Value;
                    _UID = sessionid;
                }
               
            }
            catch { }

            if (sessionid == "")
            {
                sessionid = UID;
                System.Net.Cookie SessionId = new System.Net.Cookie("sessionid", UID);
                _Context.Response.Cookies.Add(SessionId);
            }

            if (_Sessions.ContainsKey(sessionid)) { _UserSession = _Sessions[sessionid]; }
            else
            {
                _UserSession = new Elisyan.Session();
                _Sessions.Add(sessionid, _UserSession);
            }

            Console.WriteLine(UID);

            _RequestedUrl = _Context.Request.Url.OriginalString;
            _RequestedPageName = _Context.Request.Url.LocalPath;
            if (_Context.Request.HttpMethod.ToLower() == "post")
            {
                _PostRequest = true;
                foreach (string l in _Context.Request.QueryString.AllKeys)
                { Console.WriteLine(_Context.Request.QueryString[l]); }
                Console.WriteLine(_Context.Request.RawUrl);
                List<byte> Data = new List<byte>();
                byte[] array = new byte[1024];
                int cb = 0;
                while (cb < _Context.Request.ContentLength64)
                {
                    int count = 0;
                    count = _Context.Request.InputStream.Read(array, 0, array.Length);
                    for (int n = 0; n < count; n++)
                    {
                        Data.Add(array[n]);
                    }
                    cb += count;
                }



                string text = System.Text.Encoding.UTF8.GetString(Data.ToArray());
                _Variables.Clear();
                StringBuilder builder = new StringBuilder();
                {
                    int n = 0;
                    for (; n < text.Length; n++)
                    {
                        builder = new StringBuilder();
                        for (; n < text.Length; n++)
                        {
                            if (text[n] == '=') { n++; break; }
                            builder.Append(text[n]);
                        }
                        string variable = builder.ToString();
                        builder = new StringBuilder();
                        for (; n < text.Length; n++)
                        {
                            if (text[n] == '&') { n++; break; }
                            builder.Append(text[n]);
                        }
                        string value = builder.ToString();
                        if (!_Variables.ContainsKey(variable))
                        {
                            _Variables.Add(variable, System.Uri.UnescapeDataString(value));
                        }
                    }
                }
                foreach (string _ in _Variables.Keys) { Console.WriteLine(_); Console.WriteLine(_Variables[_]); }
            }
            
     
        }
        public void SendResponse(string Response)
        {
            byte[] array = System.Text.Encoding.UTF8.GetBytes(Response);
            _Context.Response.ContentLength64 = array.Length;
            _Context.Response.OutputStream.Write(array, 0, array.Length);
            try { _Context.Response.Close(); }
            catch { }
        }
        public void SendResponse(byte[] Response, string ContentType)
        {
            _Context.Response.ContentLength64 = Response.Length;
            _Context.Response.ContentType = ContentType;
            _Context.Response.OutputStream.Write(Response, 0, Response.Length);
            try { _Context.Response.Close(); }
            catch { }
        }
        public void SendResponse(string Response, string ContentType)
        {
            byte[] array = System.Text.Encoding.UTF8.GetBytes(Response);
            _Context.Response.ContentLength64 = array.Length;
            _Context.Response.ContentType = ContentType;
            _Context.Response.OutputStream.Write(array, 0, array.Length);
            try { _Context.Response.Close(); }
            catch { }

        }
    }
}
